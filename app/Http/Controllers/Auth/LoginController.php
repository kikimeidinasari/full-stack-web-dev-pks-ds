<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    
    public function __invoke(Request $request)
    {
        
        $allRequest = $request->all();

        //set validation
        $validator = Validator::make($allRequest, [
            'email'   => 'required',
            'password'   => 'required'
            
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $credentials = request(['email', 'password']);
        if(!$token = auth()->attempt($credentials)){
            return response()->json([
                'success' => false,
                'message' => 'Email atau Password tidak ditemukan'
            ], 401);
        }

        return response()->json([
            'success' => true,
            'message' => 'User berhasil login',
            'data' => [
                'user' => auth()->user(),
                'token' => $token
            ]
        ], 401);
        
    }
}
