<?php

namespace App;

use App\Role;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;


class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    protected $fillable = ['username','email','name','role_id','password','email_verified_at'];
    protected $primaryKey= 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();
        static::creating(function($model){
            // if(empty($model->id)){
            //     $model->id = Str::uuid();
            // }
            if(empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} =  Str::uuid();
            }
            $model->role_id = Role::where('name','author')->first()->id;
        });
    }
    
    public function role(){
        return $this->belongsTo('App\Role');
    }

    public function otp_code()
    {
        return $this->hasOne('App\OtpCode');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function posts()
    {
        return $this->hasMany('App\Post');
    }

}
