<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;


class comment extends Model
{
    protected $fillable = [
        'id',
        'content',
        'post_id',
        'user_id'
    ];
     protected $KeyType = 'string';
     protected $primaryKey = 'id';
     public $incrementing = false;

     protected static function boot()
    {
         parent::boot();

         static::creating(function($model){
             if ( empty($model->{$model->getKeyName()})){
                 $model->{$model->getKeyName()} = Str::uuid();
             }
             $model->user_id = auth()->user()->id;
         });
    }
    public function post()
    {
        return $this->belongsTo('App\Post');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
