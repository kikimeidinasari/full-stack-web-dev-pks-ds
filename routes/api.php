<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * route resource post
 */
Route::resource('/Post', 'PostController');
Route::resource('/comment', 'CommentController');
Route::resource('/Role', 'RoleController');

Route::group([
    'prefix' => 'auth',
    'namespace' => 'Auth'
] , function(){
    Route::post('register','RegisterController')->name('auth.register');
    Route::post('regenerate-otp-code','RegenerateOtpCodeController')->name('auth.regenerate-otp-code');
    Route::post('verification','VerificationController')->name('auth.verification');
    Route::post('update-password','UpdatePasswordController')->name('auth.update-password');
    Route::post('login','LoginController')->name('auth.login');
});

Route::get('/test', function(){
    return ' Silahkan masuk';

})->middleware('auth:api');




// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::get('/test', function(){
//     return 'Selamat Datang di Aplikasi Web Service Pertama Saya';
// });

